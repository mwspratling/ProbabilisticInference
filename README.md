----------------------------------------------------------------------------
# INTRODUCTION
----------------------------------------------------------------------------

This code implements the simulation results reported in:

[M. W. Spratling (2016) A neural implementation of Bayesian inference based on predictive coding. Connection Science, 28(4):346-83.](https://nms.kcl.ac.uk/michael.spratling/Doc/pcbc_prob.pdf)

Please cite this paper if this code is used in, or to motivate, any publications. 

----------------------------------------------------------------------------
# USAGE
----------------------------------------------------------------------------

This code was tested with MATLAB Version 8.3.0.532 (R2014a).

Some functions make use of the nansuite toolbox by Jan Glaescher, available at
http://www.mathworks.com/matlabcentral/fileexchange/6837. These functions should
thus be installed somewhere on your matlab path. Alternatively, the Statistics
Toolbox can be used. 

To use this software:
```
    start matlab 
    move to the directory containing this code 
    run one of the functions with the name pcbc_prob_*
```
Each of the pcbc_prob_* functions runs an experiment reported in the above
publication. The following table gives details of which MATLAB function produces
each figure in this article.

Figure    |  MATLAB Function
----------|-------------------------------------------------------------
2b-e      |  pcbc_prob_1gaussian (Figs. 1, 2, 4, & 5)
3         |  pcbc_prob_1gaussian_width_height (Figs. 1, 2, 5, 6, 11, 12, 15 & 16)
4         |  pcbc_prob_Ngaussian
5b-e      |  pcbc_prob_1gaussian_prior (Figs. 1 to 4)
6         |  pcbc_prob_2integrate (Figs. 1, 2, 5, & 6)
7         |  pcbc_prob_3integrate (Figs. 1, 2, 5, & 6)
8         |  pcbc_prob_2integrate_prior (Figs. 1, 2, 5, & 6)
9         |  pcbc_prob_2integrate (Figs. 3 & 4)
10a       |  pcbc_prob_2integrate_segregate (Fig. 9)
10b       |  as Fig8a but requires value of "stdInputs" to be modified in the code
11        |  pcbc_prob_3basis
12        |  pcbc_prob_4basis (Figs. 1-4, 8-11)
14a-g     |  pcbc_prob_image_orientation (Figs. 1-7)
14h-i     |  pcbc_prob_image_orientation_prior (Figs. 1-2)
15a       |  pcbc_prob_image_orientation (Fig. 8)
15b       |  pcbc_prob_image_orientation(1) (Fig. 8) 
          |
Result not shown in paper:|
          | 
sec 3.5   |  pcbc_prob_4basis_hierarchical.m
